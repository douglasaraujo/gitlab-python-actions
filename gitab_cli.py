import gitlab
import os


class GitLabActions:
    def __init__(self):
        private_token = 'xxxxxxxxxxxxxxxxxxx'  # add your git private_token
        url = 'https://gitlab.esss.lu.se/'
        self.gl = gitlab.Gitlab(url, private_token=private_token)
        self.id_group = 788  # ID subgroup inventories in nice/guis

    def create_project(self, name):
        projects = self.gl.projects.list(include_subgroups=True,
                                         all=True,
                                         search=name)

        #  Check if the project exist on the nice/guis/inventories subgroup
        #  if yes, return the project object, else create a new one.
        for project in projects:
            if project.namespace['full_path'] == 'nice/guis/inventories':
                return project

        project = self.gl.projects.create({'name': name,
                                           'namespace_id': self.id_group,
                                           'visibility': 'public'})
        return project

    def create_gitlab_ci_variables(self, project):
        self.create_gitlab_ci_variable(project,
                                       'ANSIBLE_BECOME_PASS',
                                       'xxxxxxxxxxxxxxx',  # variable value
                                       True)

        self.create_gitlab_ci_variable(project,
                                       'ANSIBLE_SSH_PASS',
                                       'xxxxxxxxxxxxxxx',  # variable value
                                       True)

        self.create_gitlab_ci_variable(project,
                                       'ANSIBLE_SSH_USER',  # variable value
                                       'xxxxxxxxxxxxxxx')

    def create_gitlab_ci_variable(self, project, key, value, masked=False):
        if not self.ci_variable_exist(project, key):
            project.variables.create({'key': key,
                                      'value': value,
                                      'masked': masked})

    def ci_variable_exist(self, project, variable):
        for _variable in project.variables.list():
            if variable == _variable.key:
                return True
        return False

    def commit_file(self, project, file_path, author_name, author_email, msg):
        file_name = os.path.basename(file_path)
        existing_files = self.file_exist(project, file_name)
        with open(file_path, 'r') as file:
            file_content = file.read()
            if existing_files is None:
                project.files.create({'file_path': file_name,
                                      'branch': 'main',
                                      'content': file_content,
                                      'author_email': author_email,
                                      'author_name': author_name,
                                      'commit_message': msg})

            else:
                existing_files.content = file_content
                existing_files.save(branch='main',
                                    commit_message='Update ' + str(file_name))

    def file_exist(self, project, file):
        _file = None
        try:
            _file = project.files.get(file_path=file, ref='main')
            return _file
        except gitlab.GitlabGetError:
            return _file


'''
git_action = GitLabActions()
project = gi_action.create_project('myrepo_api')
git_action.create_gitlab_ci_variables(project)
git_action.commit_file(project,
                      'files/.gitlab-ci.yml',
                      'Douglas Araujo',
                      'douglas.araujo@ess.eu',
                      'Added gitlab-ci file')
'''
